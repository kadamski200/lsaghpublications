﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LsAghPublications.Utils
{
    struct Publication
    {
        // dictionary info containing table properties from dl.li-publ dd table 
        public IDictionary<String, String> Properties { get; set; }
        // info from dl.li-publ>dd>.infpubl
        public String Info { get; set; }
        // punktation is in .oncena-pkt>(.ocena-iflf .ocena-pktm)
        public String PunctationIFLF { get; set; }
        public String PunctationPKTM { get; set; }
        public string GetPunctatiomIFLF()
        {
            if (PunctationIFLF.Contains("brak") || PunctationIFLF.Contains("chwilowo"))
            {
                return null;
            }
            return PunctationIFLF;
        }

        public double? GetPunctationPKTM()
        {
            return GetPunctationFromString(PunctationPKTM);
        }

        public int? GetYearFromInfo()
        {
            try
            {
                string info = Info;
                var length = info.Length;
                string slicedYear = info.Substring(length - 5, 4);
                return Int32.Parse(slicedYear);
            }
            // any exception in case of parsing, slicing error
            catch (FormatException)
            {
                return null;
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
        }
        private double? GetPunctationFromString(String punktation)
        {
            try
            {
                String trimmed = punktation.Trim();
                int length = trimmed.Length, index = 0;
                for (index = length; index-- > 0;)
                {
                    var currentChar = trimmed[index];
                    if (!(Char.IsDigit(currentChar) || currentChar == '.' || currentChar == ','))
                    {
                        break;
                    }
                }
                String extracted = trimmed.Substring(index + 1, length - index - 1);
                return Double.Parse(extracted);

            }
            catch (FormatException)
            {
                return null;
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
        }
    }
}
