﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Html = HtmlAgilityPack;

namespace LsAghPublications.Utils
{
    static class AppUtils
    {
        public static int CountPages(Html.HtmlDocument document)
        {
            try
            {
                var pagesContainerFirst = document.DocumentNode
                   .SelectSingleNode(".//form[@id='lpubl']//div[1]//fieldset[2]");
                /*var secondDiv = pagesContainerFirst
                   .SelectNodes(".//div")[1];
                var secondFieldset = secondDiv
                    .SelectNodes(".//fieldset")[1]; */
                int pagesCount = pagesContainerFirst.ChildNodes.Count;
                return pagesCount;
            }
            catch(Exception)
            {
                return 1;
            } 
           
        }
        public static IList<Publication> ExtractPaginatedPublications(Html.HtmlDocument document, String baseUrl, String authorId, String urlTemplate)
        {
            int pagesCount = CountPages(document);
            Func<String ,IList<Publication>> downloadFunction = (String currentUrl) =>
            {
                Html.HtmlDocument docPage = WebUtils.DownloadDocument(currentUrl);
                IList<Publication> result = AppUtils.ExcractPublications(docPage, baseUrl).ToList();
                return result;
            };

            IList<Task<IList<Publication>>> taskList = new List<Task<IList<Publication>>>();
            for(int currPage = 2; currPage < pagesCount+1; ++currPage)
            {
                String currentUrl = urlTemplate
                    .Replace("XXXXXX", authorId)
                    .Replace("YYYYYY", currPage.ToString());
                Task<IList<Publication>> taskPublication = Task.Factory.StartNew(() => downloadFunction(currentUrl));
                taskList.Add(taskPublication);
            }
            var taskAll = Task.WhenAll(taskList);
            var resultFromCurrentDocument = AppUtils.ExcractPublications(document, baseUrl).ToList();
            taskAll.Wait();
            return taskAll.Result.Concat(new[] {resultFromCurrentDocument }).SelectMany(x => x).ToList();
        }

        public static IEnumerable<Publication> ExcractPublications(Html.HtmlDocument document, String baseUrl)
        {
            var documentNode = document.DocumentNode;
            var definitionListNode = documentNode.SelectSingleNode(".//dl[contains(@class, 'li-publ')]");
            var definitionDataNodes = definitionListNode.SelectNodes("./dd");
            foreach (var currentDefinitionNode in definitionDataNodes)
            {
                var tableBody = currentDefinitionNode.SelectSingleNode(".//table/tbody");
                var tableRowElements = tableBody.SelectNodes(".//tr");
                var infoText = currentDefinitionNode.SelectSingleNode(".//li[contains(@class,'infpubl')]").InnerText;
                var infoAnchorContainerNode = currentDefinitionNode.SelectSingleNode(".//li[contains(@class, 'publ-info')]");
                var infoAnchorNode = infoAnchorContainerNode.SelectNodes(".//a")[0]; // [contains(@class, 'ui-tabs-anchor')]
                string urlPart = infoAnchorNode.GetAttributeValue("href", (string)null).Replace("&amp;", "&");
                var docToSearchDegrees = WebUtils.DownloadDocument(baseUrl + urlPart).DocumentNode;
                var degreeIFLFNode = docToSearchDegrees.SelectSingleNode(".//p[contains(@class, 'ocena-iflf')]");
                var degreePKTMNode = docToSearchDegrees.SelectSingleNode(".//p[contains(@class, 'ocena-pktm')]");
                string degreeIFLF = ReferenceEquals(degreeIFLFNode, null) ? "" : degreeIFLFNode.InnerText,
                       degreePKTM = ReferenceEquals(degreePKTMNode, null) ? "" : degreePKTMNode.InnerText;
                IDictionary<String, String> details = new SortedDictionary<String, String>();
                foreach (var tableRow in tableRowElements)
                {
                    var keyElement = tableRow.FirstChild;
                    var contentElement = tableRow.ChildNodes[1];
                    details.Add(new KeyValuePair<String, String>(keyElement.InnerText, contentElement.InnerText));
                }
                var publication = new Publication()
                {
                    Info = infoText,
                    Properties = details,
                    PunctationIFLF = degreeIFLF,
                    PunctationPKTM = degreePKTM

                };
                yield return publication;
            }
        }
    }
}
