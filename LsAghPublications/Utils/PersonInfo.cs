﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LsAghPublications.Utils
{
    struct PersonInfo
    {
        public String UrlPart { get; set; }
        public String Name { get; set; }
        public String Faculty { get; set; }
        public String GetId()
        {
            return this.UrlPart.Substring(this.UrlPart.LastIndexOf("-") + 1);
        }

        public String Print()
        {
            return "{ " + String.Format("UrlPart: {0}, Name: {1}, Faculty: {2}", UrlPart, Name, Faculty) + " }";
        }
    }
}
