﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LsAghPublications.Utils
{
    static class TableBuilderExtensions
    {
        public static int EnsureColumnMinimalWidth(this ITableBuilder tableBuilder, int column, int width)
        {
            int oldWidth = tableBuilder.GetColumnWidth(column);
            int updated = oldWidth > width ? oldWidth : width;
            tableBuilder.SetColumnWidth(column, updated);
            return updated;
        }

        public static int EnsureRowMinimalHeight(this ITableBuilder tableBuilder, int row, int height)
        {
            int oldHeight = tableBuilder.GetRowHeight(row);
            int updated = oldHeight > height ? oldHeight : height;
            tableBuilder.SetRowHeight(row, updated);
            return updated;
        }
    }
}
