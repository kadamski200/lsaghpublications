﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Html = HtmlAgilityPack;
using System.Net;
namespace LsAghPublications.Utils
{
    public static class WebUtils
    {
        public static Html.HtmlDocument DownloadDocument(String url)
        {
            Html.HtmlDocument document = new Html.HtmlDocument();
            HttpWebRequest oReq = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse resp = (HttpWebResponse)oReq.GetResponse();
            document.Load(resp.GetResponseStream(), Encoding.UTF8);
            return document;
        }

        
    }
}
