﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserModel = NPOI.SS.UserModel;

namespace LsAghPublications.Utils
{
    class ExcelTableWriter :  ITableBuilder
    {
        public ExcelTableWriter(UserModel.ISheet sheet)
        {
            this.sheet = sheet;
        }
        private UserModel.ISheet sheet;
        int rowCount = 0;
        int maxColumnCount = 0;

        public int RowCount
        {
            get
            {
                return rowCount;
            }
        }

        public int ColumnCount
        {
            get
            {
                return maxColumnCount;
            }
        }

        public int AddRow()
        {
            int result = this.rowCount;
            this.sheet.CreateRow(result);
            return result;
        }

        public int AddCell(int row)
        {
            if(row >= this.rowCount)
            {
                throw new IndexOutOfRangeException("Row at this index is not created yet.");
            }
            UserModel.IRow sheetRow = this.sheet.GetRow(row);
            int lastCellIndex = sheetRow.LastCellNum;
            int newCellNumber = lastCellIndex + 1;
            int newColumnCount = lastCellIndex + 1;
            sheetRow.CreateCell(newCellNumber);
            this.maxColumnCount = this.maxColumnCount > newColumnCount ? this.maxColumnCount : newColumnCount;
            return newCellNumber;
        }

        public void SetCellValue(int row, int column, string value)
        {
            UserModel.ICell sheetCell = GetCellWithIndexCheck(row, column);
            sheetCell.SetCellValue(value);
        }

        public string GetCellValue(int row, int column)
        {
            UserModel.ICell sheetCell = GetCellWithIndexCheck(row, column);
            return sheetCell.StringCellValue;
        }

        public void SetRowHeight(int row, int height)
        {
            if (row >= this.rowCount)
            {
                throw new IndexOutOfRangeException("Row at this index is not created yet.");
            }
            this.sheet.GetRow(row).Height = (short)height;
        }

        public void SetColumnWidth(int column, int width)
        {
            if(this.maxColumnCount >= column)
            {
                throw new IndexOutOfRangeException("Column at this index is not created yet.");
            }
            this.sheet.SetColumnWidth(column, width);
        }

        public int GetRowHeight(int row)
        {
            if (row >= this.rowCount)
            {
                throw new IndexOutOfRangeException("Row at this index is not created yet.");
            }
            return this.sheet.GetRow(row).Height;
        }

        public int GetColumnWidth(int column)
        {
            if (this.maxColumnCount >= column)
            {
                throw new IndexOutOfRangeException("Column at this index is not created yet.");
            }
            return this.sheet.GetColumnWidth(column);
        }

        private UserModel.ICell GetCellWithIndexCheck(int row, int column)
        {
            if (row >= this.rowCount || column >= this.maxColumnCount)
            {
                throw new IndexOutOfRangeException("row or column index is out of maximum range");
            }
            UserModel.IRow sheetRow = this.sheet.GetRow(row);
            if (column > sheetRow.LastCellNum)
            {
                throw new IndexOutOfRangeException("selected column for row does not exists");
            }
            return sheetRow.GetCell(column);
        }
    }
}
