﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LsAghPublications.Utils
{
    interface ITableBuilder
    {
        int AddRow();
        int AddCell(int row);
        void SetCellValue(int row, int column, String value);
        String GetCellValue(int row, int column);
        void SetRowHeight(int row, int height);
        void SetColumnWidth(int column, int width);
        int GetRowHeight(int row);
        int GetColumnWidth(int column);
        int RowCount { get; }
        int ColumnCount { get; }
    }
}
