﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using Html = HtmlAgilityPack;
using LsAghPublications.Utils;
using UserModel = NPOI.SS.UserModel;
using ExcelUserModel = NPOI.XSSF.UserModel;
using System.IO;

namespace LsAghPublications
{
    public partial class MainWindowForm : Form
    {

        public MainWindowForm()
        {
            InitializeComponent();
            this.lettersSet = new HashSet<String>(letters);
            this.cachedPersonDetails = new Dictionary<String, IList<PersonInfo>>();
            this.btnSave.Click += BtnSave_Click;
            this.cbLetter.SelectedIndexChanged += CbLetter_SelectedIndexChanged;
            this.cbLetter.DataSource = this.letters;
        }

        private void CbLetter_SelectedIndexChanged(object sender, EventArgs e)
        {
            var key = (String)this.cbLetter.SelectedValue;
            if (String.IsNullOrWhiteSpace(key)) { return;  }
            IList<PersonInfo> currList;
            bool isinMap = this.cachedPersonDetails.TryGetValue(key, out currList);
            if (!isinMap)
            {
                currList = this.DownloadPersonDetails(key);
                this.cachedPersonDetails.Add(new KeyValuePair<String, IList<PersonInfo>>(key, currList));
                this.currentPersonDetails = currList;
            }
            this.cbPerson.DataSource = currList;
            this.cbPerson.ValueMember = "Name";
            this.cbPerson.DisplayMember = "Name";
            try
            {
                this.cbPerson.SelectedIndex = 0;
            }
            catch (IndexOutOfRangeException) { }
        }

        

        private void WriteDictionaryEntryToSheet(UserModel.ISheet sheet, int rowNumber, String key, String value)
        {
            var row = sheet.CreateRow(rowNumber);
            row.CreateCell(0).SetCellValue(key);
            row.CreateCell(1).SetCellValue(value);
        }

        private void WriteEnumerableToSheet(UserModel.ISheet sheet, int rowNumber, IEnumerable<String> dataSource, int startIndex = 0)
        {
            var row = sheet.CreateRow(rowNumber);
            for (int i = 0; i != startIndex; i++) 
            { 
                row.CreateCell(rowNumber); 
            }
            foreach (var item in dataSource)
            {
                row.CreateCell(startIndex++).SetCellValue(item);
            }
        }
        
        private void MakeExcelReport(IEnumerable<Publication> publicationsSource, PersonInfo personDetails, int yearFrom, int yearTo)
        {
            FileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Excel Worksheets|*.xls;*.xlsx";
            var dialogResult = dialog.ShowDialog();
            if(dialogResult != DialogResult.OK)
            {
                return;
            }
            publicationsSource = publicationsSource.Where(pub => 
            {
                int? year = pub.GetYearFromInfo();
                if(year.HasValue)
                {
                    int yearValue = year.Value;
                    return yearValue >= yearFrom && yearValue < yearTo;
                }
                else
                {
                    return true;
                }
            });
            string resultFileName = dialog.FileName;
            ExcelUserModel.XSSFWorkbook result = new ExcelUserModel.XSSFWorkbook();
            int rowIndex = 0;
            UserModel.ISheet sheet = result.CreateSheet("Publications");
            UserModel.IRow titleRow = sheet.CreateRow(rowIndex++);
            titleRow.CreateCell(rowIndex++).SetCellValue("List of publications");
            sheet.CreateRow(rowIndex++);
            sheet.CreateRow(rowIndex++).CreateCell(0).SetCellValue("Person Details");
            WriteDictionaryEntryToSheet(sheet, rowIndex++, "Name: ", personDetails.Name);
            WriteDictionaryEntryToSheet(sheet, rowIndex++, "Faculty: ", personDetails.Faculty);
            sheet.CreateRow(rowIndex++);
            int publicationIndex = 1;
            foreach(var publication in publicationsSource)
            {
                sheet.CreateRow(rowIndex++).CreateCell(0).SetCellValue(publicationIndex.ToString() + ")");
                WriteEnumerableToSheet(sheet, rowIndex++, publication.Properties.Keys.Select(x => x + ":"), 0);
                WriteEnumerableToSheet(sheet, rowIndex++, publication.Properties.Values, 0);
                var rowForTitles = sheet.CreateRow(rowIndex++);
                rowForTitles.CreateCell(0).SetCellValue("Degree IFLF");
                rowForTitles.CreateCell(1).SetCellValue("Degree PKTM");
                var rowForDegrees = sheet.CreateRow(rowIndex++);
                var iflf = publication.GetPunctatiomIFLF();
                var pktm = publication.GetPunctationPKTM();
                rowForDegrees.CreateCell(0).SetCellValue(ReferenceEquals(iflf, null) ? "-" : iflf);
                rowForDegrees.CreateCell(1).SetCellValue(pktm.HasValue ? pktm.Value.ToString() : "-" );
                sheet.CreateRow(rowIndex++); // padding on next row
                publicationIndex++;
            }
            using (var stream = new FileStream(resultFileName, FileMode.Create, FileAccess.ReadWrite))
            {
                result.Write(stream);
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            int selectedIndex = this.cbPerson.SelectedIndex;
            if(selectedIndex == -1) return;
            PersonInfo personDetails = this.currentPersonDetails[selectedIndex];
            String idPart = personDetails.GetId();
            String toDownloadUrl = this.urlToSearchPerson
                .Replace("XXXXXX", idPart)
                .Replace("YYYYYY", (1).ToString());
            Html.HtmlDocument document =  WebUtils.DownloadDocument(toDownloadUrl);
            int fromIntValue = Decimal.ToInt32(Decimal.Round(numFrom.Value));
            int toIntValue = Decimal.ToInt32(Decimal.Round(numTo.Value));
            IEnumerable<Publication> publications = AppUtils.ExtractPaginatedPublications(document,
                this.urlAjaxBase,
                idPart,
                this.urlToSearchPerson);
            MakeExcelReport(publications, personDetails, fromIntValue, toIntValue);
        }


        private IList<PersonInfo> DownloadPersonDetails(String letter)
        {
            if (ReferenceEquals(letter, null) || letter.Length != 1 || !this.lettersSet.Contains(letter))
            {
                throw new ArgumentException("invalid letter argument");
            }
            var curr = this.urlBase + letter;
            var currList = new List<PersonInfo>();
            var document = WebUtils.DownloadDocument(curr);
            var documentNode = document.DocumentNode;
            Html.HtmlNode node = documentNode.SelectSingleNode("//table[@id='laut']");
            foreach (var currNode in node.SelectNodes(".//tr").Skip(1))
            {
                var nodeInfo = currNode.SelectSingleNode(".//a");
                String name = nodeInfo.InnerText;
                String urlPath = nodeInfo.GetAttributeValue("href", null);
                String additional = currNode.ChildNodes[1].InnerText;
                currList.Add(new PersonInfo() { Name = name, UrlPart = urlPath, Faculty = additional });
            }
            return currList;
        }
        private String[] letters =
        {
            "A", "B", "C",
            "Ć", "D", "E", "F", "G", "H", "I", "J", "K", "L",
            "Ł", "M", "N", "O", "P", "R", "S", "Ś", "T", "U","V",
            "W", "Z", "Ź", "Ż"
        };
        private ISet<String> lettersSet;
        private IDictionary<String, IList<PersonInfo>> cachedPersonDetails;
        private IList<PersonInfo> currentPersonDetails = new List<PersonInfo>();
        private String urlBase = "https://bpp.agh.edu.pl/indeks/?wydz=0&odR=0&doR=0&poz=";
        private String urlToSearchPerson = "https://bpp.agh.edu.pl/autor/?idA=XXXXXX&fodR=0&fdoR=0&fagTP=0&fagIF=0&fagPM=0&afi=YYYYYY&vt=t#vtype";
        private String urlAjaxBase = "https://bpp.agh.edu.pl/";
    }
}
